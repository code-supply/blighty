.POSIX:
.SUFFIXES:

stemcell_version=3541.5
stemcell=bosh-stemcell-${stemcell_version}-warden-boshlite-ubuntu-trusty-go_agent.tgz
cf_deployed_snapshot=cf-deployed

.PHONY: default
default: .envrc

.PHONY: pause
pause: bosh/state.json
	vm/pause

.PHONY: resume
resume: bosh/state.json
	vm/resume

.PHONY: start
start: bosh/state.json
	vm/start

.PHONY: revive
revive: bosh/state.json
	bosh/remove-current-manifest-sha-from-state
	bosh/create-env
	bosh -d cf cck

.PHONY: restore-cf
restore-cf:
	vm/restore ${cf_deployed_snapshot}

.PHONY: backup-releases
backup-releases:
	bosh/backup-releases ${stemcell_version}

.PHONY: restore-releases
restore-releases:
	bosh/restore-releases

.PHONY: cf
cf: .envrc cf/vars-store.yml

.PHONY: cf-login
cf-login: cf/vars-store.yml
	bosh/set-route
	cf/login

.PHONY: clean
clean:
	rm -f bosh/config.yml bosh/creds.yml bosh/state.json cf/vars-store.yml

.envrc: bosh/creds.yml bosh/config.yml
	cp envrc.template .envrc
	direnv allow

bosh/config.yml:
	bosh/create-alias $@

bosh/creds.yml: bosh-deployment/bosh.yml
	bosh/create-env

cf/vars-store.yml: .envrc backups/stemcells/${stemcell} cf-deployment/iaas-support/bosh-lite/cloud-config.yml
	bosh/upload-stemcell ${stemcell}
	cf/deploy
	vm/snapshot ${cf_deployed_snapshot}
	touch $@

backups/stemcells/${stemcell}:
	bosh/download-stemcell ${stemcell}

bosh/state.json:
	bosh/create-env

bosh-deployment/bosh.yml:
	git submodule update --init

cf-deployment/iaas-support/bosh-lite/cloud-config.yml:
	git submodule update --init
