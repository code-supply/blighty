# Blighty

BOSH Lite on VirtualBox is great for local development. This is a
collection of scripts to ease the set up of a BOSH director and CF
deployment on the stack.

## Dependencies

- BOSH v2 CLI
- direnv
- git
- jq
- make (any flavour)
- VirtualBox (I recommend 5.2, especially on GNU/Linux)

## Usage

```
make          # install a BOSH director
make cf-login # log in to CF using generated credentials
make pause    # pause the VM
make resume   # resume the VM
```

There are also scripts that help to avoid network usage:

```
make backup-releases  # do this after you've first installed CF, for example
make restore-releases # do this when you have a bosh director and you're about
                      # to re-deploy CF
```
